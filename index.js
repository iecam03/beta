const celeste = document.getElementById('celeste')
const violeta = document.getElementById('violeta')
const naranja = document.getElementById('naranja')
const verde = document.getElementById('verde')
const btnEmpezar = document.getElementById('btnEmpezar')
const Last_level = 10
const score = document.getElementById('score')



class Juego {
    constructor() {
        this.inicializar() 
        this.generatesequence()
        setTimeout(()=>{
            this.nextlevel()
        }, 800)
    }


    inicializar() {
    this.togglesbtnempezar()
    this.nextlevel = this.nextlevel.bind(this)
    this.choosecolor = this.choosecolor.bind(this)
    this.level = 1
    this.score = 0
    this.colors = {
        celeste,
        violeta,
        naranja,
        verde
    }
    }
    togglesbtnempezar(){
        if (btnEmpezar.classList.contains('hide')){
            btnEmpezar.classList.remove('hide')
        }else{
            btnEmpezar.classList.add('hide')
        }
    }
    generatesequence(){
        this.sequence = new Array(Last_level).fill(0).map(n => Math.floor(Math.random() * 4 ))
    }
    nextlevel(){
        this.sublevel = 0
        this.illuminatesequence()
        this.agraggeteclick()
    }
    numforcolors(num){
        switch(num){
            case 0:
                return 'celeste'
            case 1:
                return 'violeta'
            case 2:
                return 'naranja'
            case 3:
                return 'verde'
        }
    }
    colorsfornum(color){
        switch(color){
            case 'celeste':
                return 0
            case 'violeta':
                return 1
            case 'naranja':
                return 2
            case 'verde':
                return 3
        }
    }
    illuminatesequence(){
        for (let i = 0; i < this.level; i++){
            const color = this.numforcolors(this.sequence[i])
            setTimeout(()=> this.iluminatecolor(color), 1000 * i ) 
        }

    }
    iluminatecolor(color){
        this.colors[color].classList.add('light')
        setTimeout(()=> this.offcolor(color), 600)
    }
    offcolor(color){
        this.colors[color].classList.remove('light')
    }
    agraggeteclick(){
        this.colors.celeste.addEventListener('click',this.choosecolor)
        this.colors.violeta.addEventListener('click',this.choosecolor)
        this.colors.naranja.addEventListener('click',this.choosecolor)
        this.colors.verde.addEventListener('click',this.choosecolor)
    }
    deleteeventclick(){
        this.colors.celeste.removeEventListener('click',this.choosecolor)
        this.colors.violeta.removeEventListener('click',this.choosecolor)
        this.colors.naranja.removeEventListener('click',this.choosecolor)
        this.colors.verde.removeEventListener('click',this.choosecolor)
    }

    choosecolor(event){
        const colorname = event.target.dataset.color
        const numcolor = this.colorsfornum(colorname)
        this.iluminatecolor(colorname)
        if (numcolor === this.sequence[this.sublevel]){
            this.sublevel++
            this.score++
            if(this.sublevel === this.level){
                this.level++
                this.deleteeventclick()
                if(this.level === (Last_level + 1)){
                this.wingame()
                } else{
                setTimeout(this.nextlevel, 1500)
                }
            }
        }else{
            this.losegame()
        }
    }
    wingame(){
        swal('Nice Job!','Congratulations!,You Win Dude!', "success")
        .then(this.inicializar())
    }
    losegame(){
        swal('Ops!','Sorry you lose! try again!', "error")
        .then(()=>{
            this.deleteeventclick()
            this.inicializar()
        })
    }
}



function empezarJuego() {
    window.juego = new Juego()
}